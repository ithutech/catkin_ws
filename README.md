# catkin_ws
Follow step by step bellow:

1. `rm -rf ~/catkin_ws`
2. `cd ~/`
3. `git clone https://gitlab.com/ithutech/catkin_ws.git`
4. `cd catkin_ws`
5. `catkin_make`
6. `source devel/setup.bash`
7. `roslaunch team412 init.launch`
8. Run simulator of: `team412 - port: 9090`
