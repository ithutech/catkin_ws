import numpy as np
import cv2
import matplotlib.pyplot as plt
import time

d = os.path.dirname(os.path.abspath(__file__))
filepath = os.path.join(d, "src/team412/src/car.xml")
car_cascade = cv2.CascadeClassifier(filepath)
#car_cascade = cv2.CascadeClassifier('$PWD/src/team412/src/car.xml')
def cascade_get_car_position(image):
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    cars = car_cascade.detectMultiScale(gray, scaleFactor=1.06, minNeighbors=1, minSize=(40, 40))
    return cars

